const template = document.createElement("template");

template.innerHTML = `
<style>
*,
*::after,
*::before {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}
 .sr-only:not(:focus):not(:active) {
    clip: rect(0 0 0 0); 
    clip-path: inset(50%);
    height: 1px;
    overflow: hidden;
    position: absolute;
    white-space: nowrap; 
    width: 1px;
  }
  .user-card {
    background-color: hsl(0, 0%, 100%);
    color:hsl(229, 23%, 23%);
    border-radius: 1rem;
  }
   

.card-wrapper {
  width:24rem;
  height: min(calc(100vh - 8rem), 24rem);
  border-bottom: 1px solid hsl(0, 0%, 59%);
  border-radius: 1rem ;
  text-align:center;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column
}

.card-bg {
  object-fit: cover;
  width: 100%;
  display: block;
  object-position: center; 
  border-radius:1rem 1rem  0 0 ;
 }


 .user-profile {
  margin-top: -6rem;
}

.user-img {
  border: .3em solid hsl(0, 0%, 100%);
  border-radius: 100%;
}

.profile-info {
  display:flex;
  align-items:center;
  gap: .3em;
  font-size:1.1rem;
}
.user-name {
  font-weight: 700;
  color: hsl(229, 23%, 23%);
  text-align: center;
}
.user-age {
  color: hsl(0, 0%, 59%);
  margin-left: 0.6rem;
  font-weight: 400;
}


.user-location {
  text-align: center;
  color: hsl(0, 0%, 49%);
  padding-block-start:1rem;
  font-size: 1rem;
}
.card-state {
  display: flex;
  gap:1rem;
  align-items: center;
  justify-content: space-between;
  padding-block:1rem;
  width:80%;
  border-top: 1px solid hsl(0, 0%, 59%);
}

.card-state span {
  display: block;
  font-weight: 700;
  color: hsl(229, 23%, 23%);
  padding-block-end:0.5rem;
  font-size:1.3rem;
}
#toggleRandomUser {
  position: fixed;
  bottom: 1rem;
  left: 50%;
  transform: translateX(-50%);
  border: .1em solid hsl(229, 23%, 23%);
  padding: 0.7em 1.3em;
  font-size: 1rem;
  font-weight: 700;
  background-color: hsl(0, 0%, 100%);
  color: hsl(185, 75%, 39%);
  border-radius:1rem;
  cursor: pointer;
}

</style>
<main>
<section class="user-card">
<h1 class="sr-only">Generate Random User Profile</h1>
<div class="card-wrapper">

  <img class="card-bg" src="./assets/images/bg-pattern-card.svg"alt="" />

  <div class="user-profile">
    <div>
      <img class="user-img" src="./assets/images/image-victor.jpg" alt="Victor Crest" />
    </div>
 <div class="profile-info"> <h2 class="user-name">Victor Crest</h2><span class="user-age">26</span></div>
    <p class="user-location">London</p>
  </div>
  <div class="card-state">
    <p><span > 80k </span> Followers</p>
    <p><span > 803k</span> Likes</p>
    <p><span >1.4k </span>Photos</p>
  </div>
</div>

</section>
<button type="button" id="toggleRandomUser">Get Random User</button>
</main>
`;

class UserCard extends HTMLElement {
  static get observedAttributes() {
    return ["c", "l"];
  }

  constructor() {
    super();
  }

  connectedCallback() {
    this.attachShadow({ mode: "open" });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    const userImg = this.shadowRoot.querySelector(".user-img").src;
    const userName = this.shadowRoot.querySelector(".user-name").textContent;
    const userAge = this.shadowRoot.querySelector(".user-age").textContent;
    const userLocation =
      this.shadowRoot.querySelector(".user-location").textContent;

    this.shadowRoot
      .querySelector("#toggleRandomUser")
      .addEventListener("click", (e) => {
        this.getTest(userImg, userName, userAge, userLocation);
        this.getRandomUserData(userImg, userName, userAge, userLocation);
      });
  }
  attributeChangedCallback(attrName, oldVal, newVal) {}

  getRandomUserData(imgSrc, name, age, location) {
    let randomUserData = {};
    fetch("https://randomuser.me/api/")
      .then((res) => res.json())
      .then((data) => {
        randomUserData = data;
        let results = randomUserData.results;
        results.forEach((result) => {
          name = `${result.name.first} ${result.name.last}`;
          age = toString(result.dob.age);
          location = result.location.country;
          imgSrc = result.picture.medium;
        });
      })
      .catch((err) => console.log(err));
  }

  getTest(img, name, age, location) {
    console.log(img);
    console.log(name);
    console.log(age);
    console.log(location);
  }
}

window.customElements.define("user-card", UserCard);
