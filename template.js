/*------------------------------------------------------------------------
  // I'd like to define a Webcomponent in a Javascript-Module and call a method of this component.

  Problem is: Modules are alway loaded asyc and are alway executed after the DOM is loaded. Therefore the Web-Component is not yet defined when it's instanciated in HTML.

-------------------------------------------------------------------------*/
